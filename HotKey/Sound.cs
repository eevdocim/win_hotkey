﻿using NAudio.Wave;
using System;
using System.Windows.Forms;

namespace HotKey
{
    public static class Sound
    {
        private static WaveOutEvent outputDevice;
        private static AudioFileReader audioFile;

        private static void OnPlaybackStopped(object sender, StoppedEventArgs args)
        {
            outputDevice.Dispose();
            outputDevice = null;
            audioFile.Dispose();
            audioFile = null;
        }

        public static void Play()
        {
            try
            {
                if (outputDevice == null)
                {
                    outputDevice = new WaveOutEvent();
                    outputDevice.PlaybackStopped += OnPlaybackStopped;
                    SetVolume(Settings.data.TimerMusicVolume);
                }
                if (audioFile == null)
                {
                    audioFile = new AudioFileReader(@Settings.data.TimerMusicUrl);
                    outputDevice.Init(audioFile);
                }
                outputDevice.Play();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error:" + ex, "OK");
            }
        }

        public static void SetVolume(int volume)
        {
            if(volume < 0)
            {
                volume = 0;
            }
            else if(volume > 100) 
            {
                volume = 100;
            }
            outputDevice.Volume = volume / 100f;
        }

        public static void Stop()
        {
            outputDevice?.Stop();
        }
    }
}

﻿using System;
using System.IO;
using System.Text.Json;
using System.Windows.Forms;

namespace HotKey
{
    public static class Settings
    {
        private const string URL_CONF = "settings.json";
        public const int COUNT_BUTTONS = 12;
        public static IntPtr handle;
        public static Form wTimer;
        public static Button[] active_buttons = new Button[COUNT_BUTTONS];
        public static TextBox[] text_boxes = new TextBox[COUNT_BUTTONS];
        public static bool isWheel = false;
        public static bool isLanguage = false;
        public static bool isExit = false;
        public static TimerMethod timer_method = TimerMethod.Started;
        public static int timer_pause = 10;
        public static int timer_time = 0;
        public static bool timer_trigger = false;
        public static Data data = new Data();
        

        public enum TimerMethod
        {
            Started,
            Notifications,
            Call,
            Wait,

        }

        static Settings(){
            Load();
            wTimer = new Timer();
        }

        public static void Save()
        {
            using (StreamWriter sw = new StreamWriter(URL_CONF))
            {
                sw.WriteLine(JsonSerializer.Serialize(data));
                sw.Close();
            }
        }

        public static void Load()
        {
            try
            {
                using (StreamReader sr = new StreamReader(URL_CONF))
                {
                    data = JsonSerializer.Deserialize<Data>(sr.ReadLine());
                    sr.Close();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Save();
            }
        }

        public class Data
        {
            public string[] AppName { get; set; }
            public string[] AppUrl { get; set; }
            public bool[] AppActive { get; set; }
            public bool IsMouseVolumeControl { get; set; }
            public bool IsActiveGameMode { get; set; }
            public bool IsCapsSwitchLang { get; set; }
            public bool IsTimerEnabled { get; set; }
            public string TimerMusicUrl { get; set; }
            public int TimerMusicVolume { get; set; }
            public int TimerStartHour { get; set; }
            public int TimerStartMinute { get; set; }
            public int TimerEndHour { get; set; }
            public int TimerEndMinute { get; set; }

            public Data()
            {
                AppActive = new bool[COUNT_BUTTONS];
                AppName = new string[COUNT_BUTTONS];
                AppUrl = new string[COUNT_BUTTONS];

                for (int i = 0; i < COUNT_BUTTONS; i++)
                {
                    AppActive[i] = false;
                    AppName[i] = "";
                    AppUrl[i] = "";
                }

                IsMouseVolumeControl = false;
                IsActiveGameMode = false;
                IsCapsSwitchLang = false;
                IsTimerEnabled = false;
                TimerMusicUrl = string.Empty;
                TimerMusicVolume = 50;
                TimerStartHour = 9;
                TimerStartMinute = 50;
                TimerEndHour = 17;
                TimerEndMinute = 0;
            }
        }
    }
}

﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.Toolkit.Uwp.Notifications;

namespace HotKey
{
    public partial class Root : Form
    {
        public Root()
        {
            InitializeComponent();
            Settings.handle = Handle;
            for (int i = 0; i < Settings.COUNT_BUTTONS; i++)
            {
                Settings.active_buttons[i] = (Button)Controls["F" + (i + 1) + "_on"];
                Settings.text_boxes[i] = (TextBox)Controls["F" + (i + 1) + "_BOX"];
            }

            for (int i = 0; i < Settings.COUNT_BUTTONS; i++)
            {
                if (Settings.data.AppActive[i])
                {
                    Settings.active_buttons[i].Text = "ON";
                    Settings.active_buttons[i].BackColor = Color.Green;
                }

                Settings.text_boxes[i].Text = Settings.data.AppName[i];
            }

            if (Settings.data.IsMouseVolumeControl)
            {
                volume_on.Text = "ON";
                volume_on.BackColor = Color.Green;
            }

            if (Settings.data.IsCapsSwitchLang)
            {
                caps_switch_lang.Text = "ON";
                caps_switch_lang.BackColor = Color.Green;
            }

            if (Settings.data.IsActiveGameMode)
            {
                GM_Button.BackColor = Color.Green;
            }

            Commands.Start();
        }

        //включение перехвата win key
        private void button1_Click(object sender, EventArgs e)
        {
            if (Settings.data.IsActiveGameMode)
            {
                Settings.data.IsActiveGameMode = false;
                GM_Button.BackColor = Color.Red;
                Settings.Save();
            }
            else
            {
                Settings.data.IsActiveGameMode = true;
                GM_Button.BackColor = Color.Green;
                Settings.Save();
            }
        }
       
        //Включение перехвата мышки
        private void volume_on_Click(object sender, EventArgs e)
        {
            if (Settings.data.IsMouseVolumeControl)
            {
                Settings.data.IsMouseVolumeControl = false;
                volume_on.Text = "OFF";
                volume_on.BackColor = Color.Red;
                Settings.Save();
            }
            else
            {
                Settings.data.IsMouseVolumeControl = true;
                volume_on.Text = "ON";
                volume_on.BackColor = Color.Green;
                Settings.Save();
            }
        }

        private void ActiveHook(int number_key)
        {
            if (Settings.data.AppActive[number_key])
            {
                Settings.data.AppActive[number_key] = false;
                Settings.active_buttons[number_key].Text = "OFF";
                Settings.active_buttons[number_key].BackColor = Color.Red;
                Settings.Save();
            }
            else
            {
                if (Settings.data.AppUrl[number_key] == String.Empty)
                {
                    OpenFile(number_key);
                }
                else
                {
                    Settings.data.AppActive[number_key] = true;
                    Settings.active_buttons[number_key].Text = "ON";
                    Settings.active_buttons[number_key].BackColor = Color.Green;
                    Settings.Save();
                }
            }
        }

        //включение перехвата F1
        private void F1_on_Click(object sender, EventArgs e)
        {
            ActiveHook(0);
        }
       
        //включение перехвата F2
        private void F2_on_Click(object sender, EventArgs e)
        {
            ActiveHook(1);
        }

        //включение перехвата F3
        private void F3_on_Click(object sender, EventArgs e)
        {
            ActiveHook(2);
        }
        
        //включение перехвата F4
        private void F4_on_Click(object sender, EventArgs e)
        {
            ActiveHook(3);
        }

        //включение перехвата F5
        private void F5_on_Click(object sender, EventArgs e)
        {
            ActiveHook(4);
        }

        //включение перехвата F6
        private void F6_on_Click(object sender, EventArgs e)
        {
            ActiveHook(5);
        }

        //включение перехвата F7
        private void F7_on_Click(object sender, EventArgs e)
        {
            ActiveHook(6);
        }
        
        //включение перехвата F8
        private void F8_on_Click(object sender, EventArgs e)
        {
            ActiveHook(7);
        }
        
        //включение перехвата F9
        private void F9_on_Click(object sender, EventArgs e)
        {
            ActiveHook(8);
        }
        
        //включение перехвата F10
        private void F10_on_Click(object sender, EventArgs e)
        {
            ActiveHook(9);
        }
       
        //включение перехвата F11
        private void F11_on_Click(object sender, EventArgs e)
        {
            ActiveHook(10);
        }
        
        //включение перехвата F12
        private void F12_on_Click(object sender, EventArgs e)
        {
            ActiveHook(11);
        }

        private void OpenFile(int number_key)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Application files (*.exe)|*.exe|All files (*.*)|*.*";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                Settings.data.AppUrl[number_key] = openDialog.FileName;
                Settings.data.AppActive[number_key] = true;
                Settings.active_buttons[number_key].Text = "ON";
                Settings.active_buttons[number_key].BackColor = Color.Green;
                Commands.CheckProgramm(number_key);
                Settings.text_boxes[number_key].Text = Settings.data.AppName[number_key];
                Settings.Save();
            }
        }

        //открытие файла F1
        private void F1_button_Click(object sender, EventArgs e)
        {
            OpenFile(0);
        }

        //открытие файла F2
        private void F2_button_Click(object sender, EventArgs e)
        {
            OpenFile(1);
        }

        //открытие файла F3
        private void F3_button_Click(object sender, EventArgs e)
        {
            OpenFile(2);
        }

        //открытие файла F4
        private void F4_button_Click(object sender, EventArgs e)
        {
            OpenFile(3);
        }

        //открытие файла F5
        private void F5_button_Click(object sender, EventArgs e)
        {
            OpenFile(4);
        }

        //открытие файла F6
        private void F6_button_Click(object sender, EventArgs e)
        {
            OpenFile(5);
        }

        //открытие файла F7
        private void F7_button_Click(object sender, EventArgs e)
        {
            OpenFile(6);
        }

        //открытие файла F8
        private void F8_button_Click(object sender, EventArgs e)
        {
            OpenFile(7);
        }

        //открытие файла F9
        private void F9_button_Click(object sender, EventArgs e)
        {
            OpenFile(8);
        }

        //открытие файла F10
        private void F10_button_Click(object sender, EventArgs e)
        {
            OpenFile(9);
        }

        //открытие файла F11
        private void F11_button_Click(object sender, EventArgs e)
        {
            OpenFile(10);
        }

        //открытие файла F12
        private void F12_button_Click(object sender, EventArgs e)
        {
            OpenFile(12);
        }

        private void ClearButton(int number_key)
        {
            Settings.text_boxes[number_key].Text = "";
            Settings.active_buttons[number_key].Text = "OFF";
            Settings.active_buttons[number_key].BackColor = Color.Red;
            Settings.data.AppName[number_key] = "";
            Settings.data.AppUrl[number_key] = "";
            Settings.data.AppActive[number_key] = false;
            Settings.Save();
        }

        //отчистка кнопки F1
        private void del_F1_Click(object sender, EventArgs e)
        {
            ClearButton(0);
        }

        //отчистка кнопки F2
        private void del_F2_Click(object sender, EventArgs e)
        {
            ClearButton(1);
        }

        //отчистка кнопки F3
        private void del_F3_Click(object sender, EventArgs e)
        {
            ClearButton(2);
        }

        //отчистка кнопки F4
        private void del_F4_Click(object sender, EventArgs e)
        {
            ClearButton(3);
        }

        //отчистка кнопки F5
        private void del_F5_Click(object sender, EventArgs e)
        {
            ClearButton(4);
        }

        //отчистка кнопки F6
        private void del_F6_Click(object sender, EventArgs e)
        {
            ClearButton(5);
        }

        //отчистка кнопки F7
        private void del_F7_Click(object sender, EventArgs e)
        {
            ClearButton(6);
        }

        //отчистка кнопки F8
        private void del_F8_Click(object sender, EventArgs e)
        {
            ClearButton(7);
        }

        //отчистка кнопки F9
        private void del_F9_Click(object sender, EventArgs e)
        {
            ClearButton(8);
        }

        //отчистка кнопки F10
        private void del_F10_Click(object sender, EventArgs e)
        {
            ClearButton(9);
        }

        //отчистка кнопки F11
        private void del_F11_Click(object sender, EventArgs e)
        {
            ClearButton(10);
        }

        //отчистка кнопки F12
        private void del_F12_Click(object sender, EventArgs e)
        {
            ClearButton(11);
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            Close();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Commands.Stop();
        }

        private void buttonTimer_Click(object sender, EventArgs e)
        {
            if (!Settings.wTimer.Visible)
            {
                Settings.wTimer.Visible = true;
            }
        }

        //Таймер
        private void timerClock_Tick(object sender, EventArgs e)
        {
            int m = DateTime.Now.Minute;
            int h = DateTime.Now.Hour;
    
            if (Settings.data.IsTimerEnabled)
            {
                if (h >= Settings.data.TimerStartHour && h <= Settings.data.TimerEndHour)
                {
                    if ((m == Settings.data.TimerStartMinute || m == Settings.data.TimerEndMinute) && Settings.timer_method != Settings.TimerMethod.Wait)
                    {
                        string text = string.Empty;

                        if(m == Settings.data.TimerStartMinute)
                        {
                            text = "Срочный перерыв отстань от кода и иди отдыхать :)";
                        }else if(m == Settings.data.TimerEndMinute)
                        {
                            text = "Срочно садись за код хватит пинать :)";
                        }
                        else {
                            text = "Ошибка таймера";
                        }

                        if (Settings.timer_method == Settings.TimerMethod.Notifications)
                        {
                            Settings.timer_method = Settings.TimerMethod.Call;
                            new ToastContentBuilder()
                            .AddArgument("notificationName", "Таймер")
                            .AddArgument("action", "stop")
                            .AddText(text)
                            .AddButton(new ToastButton()
                                .SetContent("Остановить")
                                .SetBackgroundActivation())
                            .Show();
                            ToastNotificationManagerCompat.OnActivated += toastArgs =>
                            {
                                ToastArguments args = ToastArguments.Parse(toastArgs.Argument);
                                switch (args.Get("action"))
                                {
                                    case "stop":
                                        Settings.timer_time = 0;
                                        Settings.timer_method = Settings.TimerMethod.Wait;
                                        Sound.Stop();
                                        break;
                                }
                            };
                        }

                        if (Settings.timer_method == Settings.TimerMethod.Call && Settings.timer_trigger)
                        {
                            Sound.Play();
                        }

                        if (Settings.timer_time >= Settings.timer_pause)
                        {
                            Settings.timer_time = 0;
                            Settings.timer_trigger = true;
                            if (Settings.timer_method != Settings.TimerMethod.Call)
                                Settings.timer_method = Settings.TimerMethod.Notifications;
                        }
                        else
                        {
                            Settings.timer_time++;
                            Settings.timer_trigger = false;
                        }

                    }
                    else
                    {
                        if (m != Settings.data.TimerStartMinute && m != Settings.data.TimerEndMinute)
                        {
                            Settings.timer_method = Settings.TimerMethod.Started;
                            ToastNotificationManagerCompat.History.Clear();
                        }
                    }
                }
            }
        }

        private void caps_switch_lang_Click(object sender, EventArgs e)
        {
            if (Settings.data.IsCapsSwitchLang)
            {
                Settings.data.IsCapsSwitchLang = false;
                caps_switch_lang.Text = "OFF";
                caps_switch_lang.BackColor = Color.Red;
                Settings.Save();
            }
            else
            {
                Settings.data.IsCapsSwitchLang = true;
                caps_switch_lang.Text = "ON";
                caps_switch_lang.BackColor = Color.Green;
                Settings.Save();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Settings.isExit)
            {
                e.Cancel = true;
            }
            if (!Settings.wTimer.Visible)
            {
                Visible = false;
            }

        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Visible = true;
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.isExit = true;
            Close();
        }
    }
}
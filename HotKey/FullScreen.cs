﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;

namespace HotKey
{
    public static class FullSceen
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowRect(IntPtr hwnd, out RECT rc);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr GetKeyboardLayout(int WindowsThreadProcessID);

        private static Process[] procs;
        public static int GetProcessID()
        {
            int pid;
            GetWindowThreadProcessId(GetForegroundWindow(), out pid);
            return pid;
        }


        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern bool PostMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        public static void ChangeLang()
        {
            PostMessage(GetForegroundWindow(), 0x0050, 2, 0);
        }

        public static bool isfull()
        {
            bool runningFullScreen = false;
            procs = Process.GetProcessesByName("explorer");
            int explorer;
            explorer = procs[0].Id; // id процесса explorer
            RECT appBounds;
            System.Drawing.Rectangle screenBounds;
            IntPtr hWnd;
            hWnd = GetForegroundWindow();
            int pid; // id активного процесса
            GetWindowThreadProcessId(hWnd, out pid);
            if (pid != explorer && hWnd != null && !hWnd.Equals(IntPtr.Zero))
            {
                GetWindowRect(hWnd, out appBounds);
                screenBounds = Screen.FromHandle(hWnd).Bounds;
                if ((appBounds.Bottom - appBounds.Top) == screenBounds.Height && (appBounds.Right - appBounds.Left) == screenBounds.Width)
                {
                    runningFullScreen = true;
                }
            }
            return (runningFullScreen);
        }
    }
}

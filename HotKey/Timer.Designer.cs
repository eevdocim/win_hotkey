﻿namespace HotKey
{
    partial class Timer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSave = new System.Windows.Forms.Button();
            this.start_Hours = new System.Windows.Forms.NumericUpDown();
            this.start_Minute = new System.Windows.Forms.NumericUpDown();
            this.end_Hours = new System.Windows.Forms.NumericUpDown();
            this.end_Minute = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSwitch = new System.Windows.Forms.Button();
            this.OpenCall = new System.Windows.Forms.Button();
            this.MUSIC_URL = new System.Windows.Forms.TextBox();
            this.VolumeMusicTrack = new System.Windows.Forms.TrackBar();
            this.VolumeMusicLabel = new System.Windows.Forms.Label();
            this.CountVolumeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.start_Hours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.start_Minute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.end_Hours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.end_Minute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VolumeMusicTrack)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(149, 212);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(112, 35);
            this.buttonSave.TabIndex = 0;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // start_Hours
            // 
            this.start_Hours.Location = new System.Drawing.Point(114, 46);
            this.start_Hours.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.start_Hours.Name = "start_Hours";
            this.start_Hours.Size = new System.Drawing.Size(69, 26);
            this.start_Hours.TabIndex = 1;
            this.start_Hours.ValueChanged += new System.EventHandler(this.start_Hours_ValueChanged);
            // 
            // start_Minute
            // 
            this.start_Minute.Location = new System.Drawing.Point(192, 46);
            this.start_Minute.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.start_Minute.Name = "start_Minute";
            this.start_Minute.Size = new System.Drawing.Size(69, 26);
            this.start_Minute.TabIndex = 2;
            this.start_Minute.ValueChanged += new System.EventHandler(this.start_Minute_ValueChanged);
            // 
            // end_Hours
            // 
            this.end_Hours.Location = new System.Drawing.Point(114, 88);
            this.end_Hours.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.end_Hours.Name = "end_Hours";
            this.end_Hours.Size = new System.Drawing.Size(69, 26);
            this.end_Hours.TabIndex = 3;
            this.end_Hours.ValueChanged += new System.EventHandler(this.end_Hours_ValueChanged);
            // 
            // end_Minute
            // 
            this.end_Minute.Location = new System.Drawing.Point(192, 86);
            this.end_Minute.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.end_Minute.Name = "end_Minute";
            this.end_Minute.Size = new System.Drawing.Size(69, 26);
            this.end_Minute.TabIndex = 4;
            this.end_Minute.ValueChanged += new System.EventHandler(this.end_Minute_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Начало";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 95);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Конец";
            // 
            // buttonSwitch
            // 
            this.buttonSwitch.Location = new System.Drawing.Point(22, 212);
            this.buttonSwitch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonSwitch.Name = "buttonSwitch";
            this.buttonSwitch.Size = new System.Drawing.Size(112, 35);
            this.buttonSwitch.TabIndex = 7;
            this.buttonSwitch.Text = "Вкл/Выкл";
            this.buttonSwitch.UseVisualStyleBackColor = true;
            this.buttonSwitch.Click += new System.EventHandler(this.button1_Click);
            // 
            // OpenCall
            // 
            this.OpenCall.Location = new System.Drawing.Point(236, 129);
            this.OpenCall.Name = "OpenCall";
            this.OpenCall.Size = new System.Drawing.Size(53, 44);
            this.OpenCall.TabIndex = 8;
            this.OpenCall.Text = "...";
            this.OpenCall.UseVisualStyleBackColor = true;
            this.OpenCall.Click += new System.EventHandler(this.OpenCall_Click);
            // 
            // MUSIC_URL
            // 
            this.MUSIC_URL.Location = new System.Drawing.Point(15, 138);
            this.MUSIC_URL.Margin = new System.Windows.Forms.Padding(6);
            this.MUSIC_URL.Name = "MUSIC_URL";
            this.MUSIC_URL.ReadOnly = true;
            this.MUSIC_URL.Size = new System.Drawing.Size(212, 26);
            this.MUSIC_URL.TabIndex = 17;
            this.MUSIC_URL.TabStop = false;
            // 
            // VolumeMusicTrack
            // 
            this.VolumeMusicTrack.AutoSize = false;
            this.VolumeMusicTrack.Location = new System.Drawing.Point(114, 173);
            this.VolumeMusicTrack.Maximum = 100;
            this.VolumeMusicTrack.Name = "VolumeMusicTrack";
            this.VolumeMusicTrack.Size = new System.Drawing.Size(163, 31);
            this.VolumeMusicTrack.TabIndex = 18;
            this.VolumeMusicTrack.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // VolumeMusicLabel
            // 
            this.VolumeMusicLabel.AutoSize = true;
            this.VolumeMusicLabel.Location = new System.Drawing.Point(18, 173);
            this.VolumeMusicLabel.Name = "VolumeMusicLabel";
            this.VolumeMusicLabel.Size = new System.Drawing.Size(90, 20);
            this.VolumeMusicLabel.TabIndex = 19;
            this.VolumeMusicLabel.Text = "Громкость";
            // 
            // CountVolumeLabel
            // 
            this.CountVolumeLabel.AutoSize = true;
            this.CountVolumeLabel.Location = new System.Drawing.Point(283, 176);
            this.CountVolumeLabel.Name = "CountVolumeLabel";
            this.CountVolumeLabel.Size = new System.Drawing.Size(41, 20);
            this.CountVolumeLabel.TabIndex = 20;
            this.CountVolumeLabel.Text = "50%";
            // 
            // Timer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(359, 261);
            this.Controls.Add(this.CountVolumeLabel);
            this.Controls.Add(this.VolumeMusicLabel);
            this.Controls.Add(this.VolumeMusicTrack);
            this.Controls.Add(this.MUSIC_URL);
            this.Controls.Add(this.OpenCall);
            this.Controls.Add(this.buttonSwitch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.end_Minute);
            this.Controls.Add(this.end_Hours);
            this.Controls.Add(this.start_Minute);
            this.Controls.Add(this.start_Hours);
            this.Controls.Add(this.buttonSave);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Timer";
            this.Text = "Settings Timer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Timer_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.start_Hours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.start_Minute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.end_Hours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.end_Minute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VolumeMusicTrack)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.NumericUpDown start_Hours;
        private System.Windows.Forms.NumericUpDown start_Minute;
        private System.Windows.Forms.NumericUpDown end_Hours;
        private System.Windows.Forms.NumericUpDown end_Minute;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSwitch;
        private System.Windows.Forms.Button OpenCall;
        private System.Windows.Forms.TextBox MUSIC_URL;
        private System.Windows.Forms.TrackBar VolumeMusicTrack;
        private System.Windows.Forms.Label VolumeMusicLabel;
        private System.Windows.Forms.Label CountVolumeLabel;
    }
}
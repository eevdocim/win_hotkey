﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

namespace HotKey
{
    public static class Commands
    {
        //название кнопки
        [StructLayout(LayoutKind.Sequential)]
        private struct KBDLLHOOKSTRUCT
        {
            public Keys key;
        }

        private struct MSLLHOOKSTRUCT
        {
#pragma warning disable CS0649
            public Point pt;
#pragma warning disable CS0649
            public int mouseData;
        }

        //работа с окнами приложений
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]

        static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, int showWindowCommand);


        //мышь
        private const int WH_MOUSE_LL = 14;// устанавливает перехват на низком уровне
        private const int WM_MOUSEWHEEL = 0x020A;//колесико прокрутки 120 вверх -120 вниз

        private static LowLevelMouseProcDelegate callback_mouse;

        private static IntPtr hHook_mouse;

        //клава
        private const int WH_KEYBOARD_LL = 13; // устанавливает перехват на низком уровне
        private const int WM_KEYDOWN = 0x0100; // нажатие клавиши 

        //звук
        private const int APPCOMMAND_VOLUME_MUTE = 0x80000;//отключение звука
        private const int APPCOMMAND_VOLUME_UP = 0xA0000;//увеличение громкости
        private const int APPCOMMAND_VOLUME_DOWN = 0x90000;//уменьшение громкости
        private const int WM_APPCOMMAND = 0x319;//выполнение команд


        //звук
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessageW(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

        //получение окна
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr GetWindow(IntPtr hWnd, int uCmd);

        //перехват клавиатуры
        private static LowLevelKeyboardProcDelegate callback_keyboard;

        private static IntPtr hHook_keyboard;

        //установка перехвата мыши 
        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelMouseProcDelegate lpfn, IntPtr hMod, int dwThreadId);
        //установка видимости курсора
        [DllImport("user32.dll")]
        private static extern int ShowCursor(bool bShow);
        //установка перехвата 
        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProcDelegate lpfn, IntPtr hMod, int dwThreadId);

        //разблокировка 
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        //Hook handle
        [DllImport("Kernel32.dll", SetLastError = true)]
        private static extern IntPtr GetModuleHandle(IntPtr lpModuleName);

        //вызов следующего захвата
        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);


        private delegate IntPtr LowLevelKeyboardProcDelegate(int nCode, IntPtr wParam, IntPtr lParam);

        private delegate IntPtr LowLevelMouseProcDelegate(int nCode, IntPtr wParam, IntPtr lParam);

        //блокировка mouse wheel
        private static IntPtr LowLevelMouseProc_wheel(int nCode, IntPtr wParam, IntPtr lParam)
        {
            MSLLHOOKSTRUCT mouseInfo = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));
            if (wParam == (IntPtr)519)
            {
                MuteVolume();
            }
            if (wParam == (IntPtr)WM_MOUSEWHEEL)
            {
                if (mouseInfo.mouseData > 0)
                {
                    UpVolume();
                }
                else
                {
                    DownVolume();
                }
                return (IntPtr)1;
            }
            return CallNextHookEx(hHook_mouse, nCode, wParam, lParam);
        }

        //перехват всей клавиатуры
        private static IntPtr LowLevelKeyboardHookProc_key(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0)
            {
                KBDLLHOOKSTRUCT keyInfo = (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(KBDLLHOOKSTRUCT));
                if (keyInfo.key == Keys.LMenu && wParam == (IntPtr)260 && Settings.data.IsMouseVolumeControl)
                {
                    if (!Settings.isWheel)
                    {
                        Settings.isWheel = true;
                        //настройка перехвата
                        callback_mouse = LowLevelMouseProc_wheel;
                        hHook_mouse = SetWindowsHookEx(WH_MOUSE_LL, callback_mouse, GetModuleHandle(IntPtr.Zero), 0);
                    }
                }
                else if (wParam == (IntPtr)257)
                {
                    if (Settings.isWheel)
                    {
                        Settings.isWheel = false;
                        UnhookWindowsHookEx(hHook_mouse);
                    }
                }

                //Нажатие кнопки
                if (wParam == (IntPtr)WM_KEYDOWN)
                {
                    //Перехват CapsLock для смены языка
                    if (Settings.data.IsCapsSwitchLang)
                    {
                        if (keyInfo.key == Keys.LShiftKey)
                        {
                            Settings.isLanguage = true;
                        }
                        if (!Settings.isLanguage)
                        {
                            if (keyInfo.key == Keys.Capital)
                            {
                                FullSceen.ChangeLang();
                                return (IntPtr)1;
                            }
                        }
                    }
                    //windows key
                    if (
                        (keyInfo.key == Keys.RWin || keyInfo.key == Keys.LWin)
                        && (FullSceen.isfull() && Settings.data.IsActiveGameMode))
                    {
                        return (IntPtr)1;
                    }
                    //увеличение громкости Numlock
                    if (keyInfo.key == Keys.Add && !Control.IsKeyLocked(Keys.NumLock))
                    {
                        UpVolume();
                        return (IntPtr)1;
                    }
                    //уменьшение громкости Numlock
                    if (keyInfo.key == Keys.Subtract && !Control.IsKeyLocked(Keys.NumLock))
                    {
                        DownVolume();
                        return (IntPtr)1;
                    }
                    //функциональные клавиши
                    try
                    {
                        int number_key = int.Parse(keyInfo.key.ToString().ToCharArray()[1].ToString()) - 1;
                        if (Settings.data.AppActive[number_key] && !FullSceen.isfull())
                        {
                            RunProgramm(number_key);
                            return (IntPtr)1;
                        }
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine("Ошибка кнопки: " + e.ToString());
                    }
                    
                }
                else if (wParam == (IntPtr)257)
                {
                    if (keyInfo.key == Keys.LShiftKey)
                    {
                        Settings.isLanguage = false;
                    }
                }
            }
            return CallNextHookEx(hHook_keyboard, nCode, wParam, lParam);
        }

        //проверка программы
        public static void CheckProgramm(int key)
        {
            try
            {
                if (Process.GetProcessesByName(Settings.data.AppName[key]).Length == 0)
                {
                    ProcessStartInfo stInfo = new ProcessStartInfo(Settings.data.AppUrl[key]);
                    Process proc = new Process();
                    proc.StartInfo = stInfo;
                    proc.Start();
                    Settings.data.AppName[key] = proc.ProcessName;
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex, "OK");
            }
        }

        //запуск программы
        public static void RunProgramm(int key)
        {
            try
            {
                if (Process.GetProcessesByName(Settings.data.AppName[key]).Length == 0)
                {
                    ProcessStartInfo stInfo = new ProcessStartInfo(Settings.data.AppUrl[key]);
                    Process proc = new Process();
                    proc.StartInfo = stInfo;
                    proc.Start();
                    Settings.data.AppName[key] = proc.ProcessName;
                }
                else
                {
                    Process[] procs = Process.GetProcessesByName(Settings.data.AppName[key]);
                    foreach (Process p in procs)
                    {
                        ShowWindow(p.MainWindowHandle, 3);
                        SetForegroundWindow(p.MainWindowHandle);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex, "OK");
            }
        }

        public static void Start()
        {
            callback_keyboard = LowLevelKeyboardHookProc_key;//вызов функции перехвата клавиши

            //настройка перехвата
            hHook_keyboard = SetWindowsHookEx(WH_KEYBOARD_LL, callback_keyboard, GetModuleHandle(IntPtr.Zero), 0);
        }

        public static void Stop()
        {
            UnhookWindowsHookEx(hHook_keyboard);
            UnhookWindowsHookEx(hHook_mouse);

        }

        public static void UpVolume()
        {
            SendMessageW(Settings.handle, WM_APPCOMMAND, Settings.handle,
                (IntPtr)APPCOMMAND_VOLUME_UP);
        }
        public static void DownVolume()
        {
            SendMessageW(Settings.handle, WM_APPCOMMAND, Settings.handle,
                (IntPtr)APPCOMMAND_VOLUME_DOWN);
        }

        public static void MuteVolume()
        {
            SendMessageW(Settings.handle, WM_APPCOMMAND, Settings.handle,
                (IntPtr)APPCOMMAND_VOLUME_MUTE);
        }
    }
}

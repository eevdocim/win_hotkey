﻿using System;
using System.Windows.Forms;

namespace HotKey
{
    public partial class Timer : Form
    {
        public Timer()
        {
            InitializeComponent();
            MUSIC_URL.Text = Settings.data.TimerMusicUrl;
            start_Hours.Text = Settings.data.TimerStartHour.ToString();
            end_Hours.Text = Settings.data.TimerEndHour.ToString();
            start_Minute.Text = Settings.data.TimerStartMinute.ToString();
            end_Minute.Text = Settings.data.TimerEndMinute.ToString();
            buttonSwitch.Text = Settings.data.IsTimerEnabled ? "Выключить" : "Включить";
            VolumeMusicTrack.Value = Settings.data.TimerMusicVolume;
            CountVolumeLabel.Text = Settings.data.TimerMusicVolume + "%";
        }
 
        private void buttonSave_Click(object sender, EventArgs e)
        {
            Settings.Save();
            Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Settings.data.IsTimerEnabled = !Settings.data.IsTimerEnabled;
            buttonSwitch.Text = Settings.data.IsTimerEnabled ? "Выключить" : "Включить";
            Settings.Save();
        }

        private void OpenCall_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "MP3 files (*.mp3)|*.mp3|WAV files (*.wav)|*.wav|All files (*.*)|*.*";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                Settings.data.TimerMusicUrl = openDialog.FileName;
                MUSIC_URL.Text = Settings.data.TimerMusicUrl;
                Settings.Save();
            }
        }

        private void Timer_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Save();
            Visible = false;
            e.Cancel = true;
        }

        private void start_Hours_ValueChanged(object sender, EventArgs e)
        {
            Settings.data.TimerStartHour = Convert.ToInt32(start_Hours.Value);
        }

        private void start_Minute_ValueChanged(object sender, EventArgs e)
        {
            Settings.data.TimerStartMinute = Convert.ToInt32(start_Minute.Value);
        }

        private void end_Hours_ValueChanged(object sender, EventArgs e)
        {
            Settings.data.TimerEndHour = Convert.ToInt32(end_Hours.Value);
        }

        private void end_Minute_ValueChanged(object sender, EventArgs e)
        {
            Settings.data.TimerEndMinute = Convert.ToInt32(end_Minute.Value);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            Settings.data.TimerMusicVolume = VolumeMusicTrack.Value;
            CountVolumeLabel.Text = Settings.data.TimerMusicVolume + "%";
        }
    }
}
